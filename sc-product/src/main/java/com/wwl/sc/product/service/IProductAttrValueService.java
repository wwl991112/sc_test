package com.wwl.sc.product.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wwl.sc.product.domain.ProductAttrValue;

/**
 * spu属性值Service接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
public interface IProductAttrValueService extends IService<ProductAttrValue> {

}

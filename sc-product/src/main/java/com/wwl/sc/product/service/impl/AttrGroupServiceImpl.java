package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.AttrGroup;
import com.wwl.sc.product.mapper.AttrGroupMapper;
import com.wwl.sc.product.service.IAttrGroupService;
import org.springframework.stereotype.Service;

/**
 * 属性分组Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupMapper, AttrGroup> implements IAttrGroupService {

}

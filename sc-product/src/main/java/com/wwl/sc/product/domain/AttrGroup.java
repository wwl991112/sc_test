package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 属性分组对象 pms_attr_group
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_attr_group")
public class AttrGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 分组id */
    @TableId
    private Long attrGroupId;

    /** 组名 */
    @Excel(name = "组名")
    private String attrGroupName;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 描述 */
    @Excel(name = "描述")
    private String descript;

    /** 组图标 */
    @Excel(name = "组图标")
    private String icon;

    /** 所属分类id */
    @Excel(name = "所属分类id")
    private Long catelogId;

}

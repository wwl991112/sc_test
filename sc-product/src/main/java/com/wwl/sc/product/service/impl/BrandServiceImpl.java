package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.Brand;
import com.wwl.sc.product.mapper.BrandMapper;
import com.wwl.sc.product.service.IBrandService;
import org.springframework.stereotype.Service;

/**
 * 品牌Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements IBrandService {

}

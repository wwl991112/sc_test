package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.SkuSaleAttrValue;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku销售属性&值Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface SkuSaleAttrValueMapper extends BaseMapper<SkuSaleAttrValue> {

}

package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.SkuImages;
import com.wwl.sc.product.mapper.SkuImagesMapper;
import com.wwl.sc.product.service.ISkuImagesService;
import org.springframework.stereotype.Service;

/**
 * sku图片Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class SkuImagesServiceImpl extends ServiceImpl<SkuImagesMapper, SkuImages> implements ISkuImagesService {

}

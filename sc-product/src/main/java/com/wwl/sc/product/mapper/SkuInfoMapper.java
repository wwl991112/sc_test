package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.SkuInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku信息Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface SkuInfoMapper extends BaseMapper<SkuInfo> {

}

package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.Attr;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface AttrMapper extends BaseMapper<Attr> {

}

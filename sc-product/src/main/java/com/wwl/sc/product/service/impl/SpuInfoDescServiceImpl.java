package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.SpuInfoDesc;
import com.wwl.sc.product.mapper.SpuInfoDescMapper;
import com.wwl.sc.product.service.ISpuInfoDescService;
import org.springframework.stereotype.Service;

/**
 * spu信息介绍Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class SpuInfoDescServiceImpl extends ServiceImpl<SpuInfoDescMapper, SpuInfoDesc> implements ISpuInfoDescService {

}

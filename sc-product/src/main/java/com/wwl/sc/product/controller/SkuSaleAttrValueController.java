package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.SkuSaleAttrValue;
import com.wwl.sc.product.service.ISkuSaleAttrValueService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * sku销售属性&值Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/value")
@Api(value = "sku销售属性&值控制器", tags = {"sku销售属性&值管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SkuSaleAttrValueController extends BaseController
{
    private final ISkuSaleAttrValueService skuSaleAttrValueService;

    /**
     * 查询sku销售属性&值列表
     */
    @ApiOperation("查询sku销售属性&值列表")
    @PreAuthorize("@ss.hasPermi('sc:value:list')")
    @GetMapping("/list")
    public TableDataInfo list(SkuSaleAttrValue skuSaleAttrValue) {
        startPage();
        List<SkuSaleAttrValue> list = skuSaleAttrValueService.list(new QueryWrapper<SkuSaleAttrValue>(skuSaleAttrValue));
        return getDataTable(list);
    }

    /**
     * 导出sku销售属性&值列表
     */
    @ApiOperation("导出sku销售属性&值列表")
    @PreAuthorize("@ss.hasPermi('sc:value:export')")
    @Log(title = "sku销售属性&值", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SkuSaleAttrValue skuSaleAttrValue) {
        List<SkuSaleAttrValue> list = skuSaleAttrValueService.list(new QueryWrapper<SkuSaleAttrValue>(skuSaleAttrValue));
        ExcelUtil<SkuSaleAttrValue> util = new ExcelUtil<SkuSaleAttrValue>(SkuSaleAttrValue.class);
        return util.exportExcel(list, "sku销售属性&值数据");
    }

    /**
     * 获取sku销售属性&值详细信息
     */
    @ApiOperation("获取sku销售属性&值详细信息")
    @PreAuthorize("@ss.hasPermi('sc:value:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(skuSaleAttrValueService.getById(id));
    }

    /**
     * 新增sku销售属性&值
     */
    @ApiOperation("新增sku销售属性&值")
    @PreAuthorize("@ss.hasPermi('sc:value:add')")
    @Log(title = "sku销售属性&值", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SkuSaleAttrValue skuSaleAttrValue) {
        return toAjax(skuSaleAttrValueService.save(skuSaleAttrValue));
    }

    /**
     * 修改sku销售属性&值
     */
    @ApiOperation("修改sku销售属性&值")
    @PreAuthorize("@ss.hasPermi('sc:value:edit')")
    @Log(title = "sku销售属性&值", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SkuSaleAttrValue skuSaleAttrValue) {
        return toAjax(skuSaleAttrValueService.updateById(skuSaleAttrValue));
    }

    /**
     * 删除sku销售属性&值
     */
    @ApiOperation("删除sku销售属性&值")
    @PreAuthorize("@ss.hasPermi('sc:value:remove')")
    @Log(title = "sku销售属性&值", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(skuSaleAttrValueService.removeByIds(Arrays.asList(ids)));
    }
}

package com.wwl.sc.product.mapper;

;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.Brand;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface BrandMapper extends BaseMapper<Brand> {

}

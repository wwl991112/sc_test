package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.AttrGroup;
import com.wwl.sc.product.service.IAttrGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 属性分组Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/group")
@Api(value = "属性分组控制器", tags = {"属性分组管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AttrGroupController extends BaseController
{
    private final IAttrGroupService attrGroupService;

    /**
     * 查询属性分组列表
     */
    @ApiOperation("查询属性分组列表")
    @PreAuthorize("@ss.hasPermi('sc:group:list')")
    @GetMapping("/list")
    public TableDataInfo list(AttrGroup attrGroup) {
        startPage();
        List<AttrGroup> list = attrGroupService.list(new QueryWrapper<AttrGroup>(attrGroup));
        return getDataTable(list);
    }

    /**
     * 导出属性分组列表
     */
    @ApiOperation("导出属性分组列表")
    @PreAuthorize("@ss.hasPermi('sc:group:export')")
    @Log(title = "属性分组", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AttrGroup attrGroup) {
        List<AttrGroup> list = attrGroupService.list(new QueryWrapper<AttrGroup>(attrGroup));
        ExcelUtil<AttrGroup> util = new ExcelUtil<AttrGroup>(AttrGroup.class);
        return util.exportExcel(list, "属性分组数据");
    }

    /**
     * 获取属性分组详细信息
     */
    @ApiOperation("获取属性分组详细信息")
    @PreAuthorize("@ss.hasPermi('sc:group:query')")
    @GetMapping(value = "/{attrGroupId}")
    public AjaxResult getInfo(@PathVariable("attrGroupId") Long attrGroupId) {
        return AjaxResult.success(attrGroupService.getById(attrGroupId));
    }

    /**
     * 新增属性分组
     */
    @ApiOperation("新增属性分组")
    @PreAuthorize("@ss.hasPermi('sc:group:add')")
    @Log(title = "属性分组", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AttrGroup attrGroup) {
        return toAjax(attrGroupService.save(attrGroup));
    }

    /**
     * 修改属性分组
     */
    @ApiOperation("修改属性分组")
    @PreAuthorize("@ss.hasPermi('sc:group:edit')")
    @Log(title = "属性分组", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AttrGroup attrGroup) {
        return toAjax(attrGroupService.updateById(attrGroup));
    }

    /**
     * 删除属性分组
     */
    @ApiOperation("删除属性分组")
    @PreAuthorize("@ss.hasPermi('sc:group:remove')")
    @Log(title = "属性分组", businessType = BusinessType.DELETE)
	@DeleteMapping("/{attrGroupIds}")
    public AjaxResult remove(@PathVariable Long[] attrGroupIds) {
        return toAjax(attrGroupService.removeByIds(Arrays.asList(attrGroupIds)));
    }
}

package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.SpuInfoDesc;
import com.wwl.sc.product.service.ISpuInfoDescService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * spu信息介绍Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/desc")
@Api(value = "spu信息介绍控制器", tags = {"spu信息介绍管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SpuInfoDescController extends BaseController
{
    private final ISpuInfoDescService spuInfoDescService;

    /**
     * 查询spu信息介绍列表
     */
    @ApiOperation("查询spu信息介绍列表")
    @PreAuthorize("@ss.hasPermi('sc:desc:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpuInfoDesc spuInfoDesc) {
        startPage();
        List<SpuInfoDesc> list = spuInfoDescService.list(new QueryWrapper<SpuInfoDesc>(spuInfoDesc));
        return getDataTable(list);
    }

    /**
     * 导出spu信息介绍列表
     */
    @ApiOperation("导出spu信息介绍列表")
    @PreAuthorize("@ss.hasPermi('sc:desc:export')")
    @Log(title = "spu信息介绍", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpuInfoDesc spuInfoDesc) {
        List<SpuInfoDesc> list = spuInfoDescService.list(new QueryWrapper<SpuInfoDesc>(spuInfoDesc));
        ExcelUtil<SpuInfoDesc> util = new ExcelUtil<SpuInfoDesc>(SpuInfoDesc.class);
        return util.exportExcel(list, "spu信息介绍数据");
    }

    /**
     * 获取spu信息介绍详细信息
     */
    @ApiOperation("获取spu信息介绍详细信息")
    @PreAuthorize("@ss.hasPermi('sc:desc:query')")
    @GetMapping(value = "/{spuId}")
    public AjaxResult getInfo(@PathVariable("spuId") Long spuId) {
        return AjaxResult.success(spuInfoDescService.getById(spuId));
    }

    /**
     * 新增spu信息介绍
     */
    @ApiOperation("新增spu信息介绍")
    @PreAuthorize("@ss.hasPermi('sc:desc:add')")
    @Log(title = "spu信息介绍", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpuInfoDesc spuInfoDesc) {
        return toAjax(spuInfoDescService.save(spuInfoDesc));
    }

    /**
     * 修改spu信息介绍
     */
    @ApiOperation("修改spu信息介绍")
    @PreAuthorize("@ss.hasPermi('sc:desc:edit')")
    @Log(title = "spu信息介绍", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpuInfoDesc spuInfoDesc) {
        return toAjax(spuInfoDescService.updateById(spuInfoDesc));
    }

    /**
     * 删除spu信息介绍
     */
    @ApiOperation("删除spu信息介绍")
    @PreAuthorize("@ss.hasPermi('sc:desc:remove')")
    @Log(title = "spu信息介绍", businessType = BusinessType.DELETE)
	@DeleteMapping("/{spuIds}")
    public AjaxResult remove(@PathVariable Long[] spuIds) {
        return toAjax(spuInfoDescService.removeByIds(Arrays.asList(spuIds)));
    }
}

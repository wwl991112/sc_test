package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.CategoryBrandRelation;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌分类关联Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface CategoryBrandRelationMapper extends BaseMapper<CategoryBrandRelation> {

}

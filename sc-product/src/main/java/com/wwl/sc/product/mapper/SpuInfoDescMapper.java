package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.SpuInfoDesc;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface SpuInfoDescMapper extends BaseMapper<SpuInfoDesc> {

}

package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.AttrGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface AttrGroupMapper extends BaseMapper<AttrGroup> {

}

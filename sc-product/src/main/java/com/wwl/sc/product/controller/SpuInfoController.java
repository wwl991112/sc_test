package com.wwl.sc.product.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.SpuInfo;
import com.wwl.sc.product.service.ISpuInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


/**
 * spu信息Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/info")
@Api(value = "spu信息控制器", tags = {"spu信息管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SpuInfoController extends BaseController
{
    private final ISpuInfoService spuInfoService;

    /**
     * 查询spu信息列表
     */
    @ApiOperation("查询spu信息列表")
    @PreAuthorize("@ss.hasPermi('sc:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpuInfo spuInfo) {
        startPage();
        List<SpuInfo> list = spuInfoService.list(new QueryWrapper<SpuInfo>(spuInfo));
        return getDataTable(list);
    }

    /**
     * 导出spu信息列表
     */
    @ApiOperation("导出spu信息列表")
    @PreAuthorize("@ss.hasPermi('sc:info:export')")
    @Log(title = "spu信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpuInfo spuInfo) {
        List<SpuInfo> list = spuInfoService.list(new QueryWrapper<SpuInfo>(spuInfo));
        ExcelUtil<SpuInfo> util = new ExcelUtil<SpuInfo>(SpuInfo.class);
        return util.exportExcel(list, "spu信息数据");
    }

    /**
     * 获取spu信息详细信息
     */
    @ApiOperation("获取spu信息详细信息")
    @PreAuthorize("@ss.hasPermi('sc:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(spuInfoService.getById(id));
    }

    /**
     * 新增spu信息
     */
    @ApiOperation("新增spu信息")
    @PreAuthorize("@ss.hasPermi('sc:info:add')")
    @Log(title = "spu信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpuInfo spuInfo) {
        return toAjax(spuInfoService.save(spuInfo));
    }

    /**
     * 修改spu信息
     */
    @ApiOperation("修改spu信息")
    @PreAuthorize("@ss.hasPermi('sc:info:edit')")
    @Log(title = "spu信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpuInfo spuInfo) {
        return toAjax(spuInfoService.updateById(spuInfo));
    }

    /**
     * 删除spu信息
     */
    @ApiOperation("删除spu信息")
    @PreAuthorize("@ss.hasPermi('sc:info:remove')")
    @Log(title = "spu信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(spuInfoService.removeByIds(Arrays.asList(ids)));
    }
}

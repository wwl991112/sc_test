package com.wwl.sc.product.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wwl.sc.product.domain.Category;

/**
 * 商品三级分类Service接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
public interface ICategoryService extends IService<Category> {

}

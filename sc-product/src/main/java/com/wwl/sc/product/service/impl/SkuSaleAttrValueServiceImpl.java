package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.SkuSaleAttrValue;
import com.wwl.sc.product.mapper.SkuSaleAttrValueMapper;
import com.wwl.sc.product.service.ISkuSaleAttrValueService;
import org.springframework.stereotype.Service;

/**
 * sku销售属性&值Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class SkuSaleAttrValueServiceImpl extends ServiceImpl<SkuSaleAttrValueMapper, SkuSaleAttrValue> implements ISkuSaleAttrValueService {

}

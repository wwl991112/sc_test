package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * spu信息介绍对象 pms_spu_info_desc
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_spu_info_desc")
public class SpuInfoDesc implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 商品id */
    private Long spuId;

    /** 商品介绍 */
    @Excel(name = "商品介绍")
    private String decript;

}

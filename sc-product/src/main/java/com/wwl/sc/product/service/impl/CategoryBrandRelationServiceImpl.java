package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.CategoryBrandRelation;
import com.wwl.sc.product.mapper.CategoryBrandRelationMapper;
import com.wwl.sc.product.service.ICategoryBrandRelationService;
import org.springframework.stereotype.Service;

/**
 * 品牌分类关联Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class CategoryBrandRelationServiceImpl extends ServiceImpl<CategoryBrandRelationMapper, CategoryBrandRelation> implements ICategoryBrandRelationService {

}

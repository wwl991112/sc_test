package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.Brand;
import com.wwl.sc.product.service.IBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 品牌Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/brand")
@Api(value = "品牌控制器", tags = {"品牌管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class BrandController extends BaseController
{
    private final IBrandService brandService;

    /**
     * 查询品牌列表
     */
    @ApiOperation("查询品牌列表")
    @PreAuthorize("@ss.hasPermi('sc:brand:list')")
    @GetMapping("/list")
    public TableDataInfo list(Brand brand) {
        startPage();
        List<Brand> list = brandService.list(new QueryWrapper<Brand>(brand));
        return getDataTable(list);
    }

    /**
     * 导出品牌列表
     */
    @ApiOperation("导出品牌列表")
    @PreAuthorize("@ss.hasPermi('sc:brand:export')")
    @Log(title = "品牌", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Brand brand) {
        List<Brand> list = brandService.list(new QueryWrapper<Brand>(brand));
        ExcelUtil<Brand> util = new ExcelUtil<Brand>(Brand.class);
        return util.exportExcel(list, "品牌数据");
    }

    /**
     * 获取品牌详细信息
     */
    @ApiOperation("获取品牌详细信息")
    @PreAuthorize("@ss.hasPermi('sc:brand:query')")
    @GetMapping(value = "/{brandId}")
    public AjaxResult getInfo(@PathVariable("brandId") Long brandId) {
        return AjaxResult.success(brandService.getById(brandId));
    }

    /**
     * 新增品牌
     */
    @ApiOperation("新增品牌")
    @PreAuthorize("@ss.hasPermi('sc:brand:add')")
    @Log(title = "品牌", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Brand brand) {
        return toAjax(brandService.save(brand));
    }

    /**
     * 修改品牌
     */
    @ApiOperation("修改品牌")
    @PreAuthorize("@ss.hasPermi('sc:brand:edit')")
    @Log(title = "品牌", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Brand brand) {
        return toAjax(brandService.updateById(brand));
    }

    /**
     * 删除品牌
     */
    @ApiOperation("删除品牌")
    @PreAuthorize("@ss.hasPermi('sc:brand:remove')")
    @Log(title = "品牌", businessType = BusinessType.DELETE)
	@DeleteMapping("/{brandIds}")
    public AjaxResult remove(@PathVariable Long[] brandIds) {
        return toAjax(brandService.removeByIds(Arrays.asList(brandIds)));
    }
}

package com.wwl.sc.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wwl.sc.product.domain.AttrAttrgroupRelation;
import org.springframework.stereotype.Service;

/**
 * 属性&属性分组关联Service接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public interface IAttrAttrgroupRelationService extends IService<AttrAttrgroupRelation> {

}

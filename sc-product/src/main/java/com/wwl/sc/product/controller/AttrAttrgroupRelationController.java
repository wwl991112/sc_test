package com.wwl.sc.product.controller;

import java.util.Arrays;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.AttrAttrgroupRelation;
import com.wwl.sc.product.service.IAttrAttrgroupRelationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 属性&属性分组关联Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/relation")
@Api(value = "属性&属性分组关联控制器", tags = {"属性&属性分组关联管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AttrAttrgroupRelationController extends BaseController
{
    private final IAttrAttrgroupRelationService attrAttrgroupRelationService;

    /**
     * 查询属性&属性分组关联列表
     */
    @ApiOperation("查询属性&属性分组关联列表")
    @PreAuthorize("@ss.hasPermi('sc:relation:list')")
    @GetMapping("/list")
    public TableDataInfo list(AttrAttrgroupRelation attrAttrgroupRelation) {
        startPage();
        List<AttrAttrgroupRelation> list = attrAttrgroupRelationService.list(new QueryWrapper<AttrAttrgroupRelation>(attrAttrgroupRelation));
        return getDataTable(list);
    }

    /**
     * 导出属性&属性分组关联列表
     */
    @ApiOperation("导出属性&属性分组关联列表")
    @PreAuthorize("@ss.hasPermi('sc:relation:export')")
    @Log(title = "属性&属性分组关联", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AttrAttrgroupRelation attrAttrgroupRelation) {
        List<AttrAttrgroupRelation> list = attrAttrgroupRelationService.list(new QueryWrapper<AttrAttrgroupRelation>(attrAttrgroupRelation));
        ExcelUtil<AttrAttrgroupRelation> util = new ExcelUtil<AttrAttrgroupRelation>(AttrAttrgroupRelation.class);
        return util.exportExcel(list, "属性&属性分组关联数据");
    }

    /**
     * 获取属性&属性分组关联详细信息
     */
    @ApiOperation("获取属性&属性分组关联详细信息")
    @PreAuthorize("@ss.hasPermi('sc:relation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(attrAttrgroupRelationService.getById(id));
    }

    /**
     * 新增属性&属性分组关联
     */
    @ApiOperation("新增属性&属性分组关联")
    @PreAuthorize("@ss.hasPermi('sc:relation:add')")
    @Log(title = "属性&属性分组关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AttrAttrgroupRelation attrAttrgroupRelation) {
        return toAjax(attrAttrgroupRelationService.save(attrAttrgroupRelation));
    }

    /**
     * 修改属性&属性分组关联
     */
    @ApiOperation("修改属性&属性分组关联")
    @PreAuthorize("@ss.hasPermi('sc:relation:edit')")
    @Log(title = "属性&属性分组关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AttrAttrgroupRelation attrAttrgroupRelation) {
        return toAjax(attrAttrgroupRelationService.updateById(attrAttrgroupRelation));
    }

    /**
     * 删除属性&属性分组关联
     */
    @ApiOperation("删除属性&属性分组关联")
    @PreAuthorize("@ss.hasPermi('sc:relation:remove')")
    @Log(title = "属性&属性分组关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(attrAttrgroupRelationService.removeByIds(Arrays.asList(ids)));
    }
}

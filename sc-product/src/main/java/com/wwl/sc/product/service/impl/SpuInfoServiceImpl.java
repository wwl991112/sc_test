package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.SpuInfo;
import com.wwl.sc.product.mapper.SpuInfoMapper;
import com.wwl.sc.product.service.ISpuInfoService;
import org.springframework.stereotype.Service;

/**
 * spu信息Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoMapper, SpuInfo> implements ISpuInfoService {

}

package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * spu图片对象 pms_spu_images
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_spu_images")
public class SpuImages implements Serializable {
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId
    private Long id;

    /** spu_id */
    @Excel(name = "spu_id")
    private Long spuId;

    /** 图片名 */
    @Excel(name = "图片名")
    private String imgName;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String imgUrl;

    /** 顺序 */
    @Excel(name = "顺序")
    private Long imgSort;

    /** 是否默认图 */
    @Excel(name = "是否默认图")
    private Long defaultImg;

}

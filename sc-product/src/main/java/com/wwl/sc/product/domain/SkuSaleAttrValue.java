package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * sku销售属性&值对象 pms_sku_sale_attr_value
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_sku_sale_attr_value")
public class SkuSaleAttrValue implements Serializable {
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId
    private Long id;

    /** sku_id */
    @Excel(name = "sku_id")
    private Long skuId;

    /** attr_id */
    @Excel(name = "attr_id")
    private Long attrId;

    /** 销售属性名 */
    @Excel(name = "销售属性名")
    private String attrName;

    /** 销售属性值 */
    @Excel(name = "销售属性值")
    private String attrValue;

    /** 顺序 */
    @Excel(name = "顺序")
    private Long attrSort;

}

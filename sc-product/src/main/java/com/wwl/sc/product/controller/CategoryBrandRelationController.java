package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.CategoryBrandRelation;
import com.wwl.sc.product.service.ICategoryBrandRelationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 品牌分类关联Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/relation")
@Api(value = "品牌分类关联控制器", tags = {"品牌分类关联管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CategoryBrandRelationController extends BaseController
{
    private final ICategoryBrandRelationService categoryBrandRelationService;

    /**
     * 查询品牌分类关联列表
     */
    @ApiOperation("查询品牌分类关联列表")
    @PreAuthorize("@ss.hasPermi('sc:relation:list')")
    @GetMapping("/list")
    public TableDataInfo list(CategoryBrandRelation categoryBrandRelation) {
        startPage();
        List<CategoryBrandRelation> list = categoryBrandRelationService.list(new QueryWrapper<CategoryBrandRelation>(categoryBrandRelation));
        return getDataTable(list);
    }

    /**
     * 导出品牌分类关联列表
     */
    @ApiOperation("导出品牌分类关联列表")
    @PreAuthorize("@ss.hasPermi('sc:relation:export')")
    @Log(title = "品牌分类关联", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CategoryBrandRelation categoryBrandRelation) {
        List<CategoryBrandRelation> list = categoryBrandRelationService.list(new QueryWrapper<CategoryBrandRelation>(categoryBrandRelation));
        ExcelUtil<CategoryBrandRelation> util = new ExcelUtil<CategoryBrandRelation>(CategoryBrandRelation.class);
        return util.exportExcel(list, "品牌分类关联数据");
    }

    /**
     * 获取品牌分类关联详细信息
     */
    @ApiOperation("获取品牌分类关联详细信息")
    @PreAuthorize("@ss.hasPermi('sc:relation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(categoryBrandRelationService.getById(id));
    }

    /**
     * 新增品牌分类关联
     */
    @ApiOperation("新增品牌分类关联")
    @PreAuthorize("@ss.hasPermi('sc:relation:add')")
    @Log(title = "品牌分类关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CategoryBrandRelation categoryBrandRelation) {
        return toAjax(categoryBrandRelationService.save(categoryBrandRelation));
    }

    /**
     * 修改品牌分类关联
     */
    @ApiOperation("修改品牌分类关联")
    @PreAuthorize("@ss.hasPermi('sc:relation:edit')")
    @Log(title = "品牌分类关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CategoryBrandRelation categoryBrandRelation) {
        return toAjax(categoryBrandRelationService.updateById(categoryBrandRelation));
    }

    /**
     * 删除品牌分类关联
     */
    @ApiOperation("删除品牌分类关联")
    @PreAuthorize("@ss.hasPermi('sc:relation:remove')")
    @Log(title = "品牌分类关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(categoryBrandRelationService.removeByIds(Arrays.asList(ids)));
    }
}

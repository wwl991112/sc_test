package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.SpuImages;
import com.wwl.sc.product.mapper.SpuImagesMapper;
import com.wwl.sc.product.service.ISpuImagesService;
import org.springframework.stereotype.Service;

/**
 * spu图片Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class SpuImagesServiceImpl extends ServiceImpl<SpuImagesMapper, SpuImages> implements ISpuImagesService {

}

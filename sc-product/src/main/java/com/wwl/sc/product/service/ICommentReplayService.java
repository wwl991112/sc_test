package com.wwl.sc.product.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wwl.sc.product.domain.CommentReplay;

/**
 * 商品评价回复关系Service接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
public interface ICommentReplayService extends IService<CommentReplay> {

}

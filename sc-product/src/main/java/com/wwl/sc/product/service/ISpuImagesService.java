package com.wwl.sc.product.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wwl.sc.product.domain.SpuImages;

/**
 * spu图片Service接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
public interface ISpuImagesService extends IService<SpuImages> {

}

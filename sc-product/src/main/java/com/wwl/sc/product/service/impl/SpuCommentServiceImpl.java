package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.SpuComment;
import com.wwl.sc.product.mapper.SpuCommentMapper;
import com.wwl.sc.product.service.ISpuCommentService;
import org.springframework.stereotype.Service;

/**
 * 商品评价Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class SpuCommentServiceImpl extends ServiceImpl<SpuCommentMapper, SpuComment> implements ISpuCommentService {

}

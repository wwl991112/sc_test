package com.wwl.sc.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.AttrAttrgroupRelation;
import com.wwl.sc.product.mapper.AttrAttrgroupRelationMapper;
import com.wwl.sc.product.service.IAttrAttrgroupRelationService;
import org.springframework.stereotype.Service;

/**
 * 属性&属性分组关联Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class AttrAttrgroupRelationServiceImpl extends ServiceImpl<AttrAttrgroupRelationMapper, AttrAttrgroupRelation> implements IAttrAttrgroupRelationService {

}

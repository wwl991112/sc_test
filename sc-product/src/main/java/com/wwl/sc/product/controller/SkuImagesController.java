package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.SkuImages;
import com.wwl.sc.product.service.ISkuImagesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * sku图片Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/images")
@Api(value = "sku图片控制器", tags = {"sku图片管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SkuImagesController extends BaseController
{
    private final ISkuImagesService skuImagesService;

    /**
     * 查询sku图片列表
     */
    @ApiOperation("查询sku图片列表")
    @PreAuthorize("@ss.hasPermi('sc:images:list')")
    @GetMapping("/list")
    public TableDataInfo list(SkuImages skuImages) {
        startPage();
        List<SkuImages> list = skuImagesService.list(new QueryWrapper<SkuImages>(skuImages));
        return getDataTable(list);
    }

    /**
     * 导出sku图片列表
     */
    @ApiOperation("导出sku图片列表")
    @PreAuthorize("@ss.hasPermi('sc:images:export')")
    @Log(title = "sku图片", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SkuImages skuImages) {
        List<SkuImages> list = skuImagesService.list(new QueryWrapper<SkuImages>(skuImages));
        ExcelUtil<SkuImages> util = new ExcelUtil<SkuImages>(SkuImages.class);
        return util.exportExcel(list, "sku图片数据");
    }

    /**
     * 获取sku图片详细信息
     */
    @ApiOperation("获取sku图片详细信息")
    @PreAuthorize("@ss.hasPermi('sc:images:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(skuImagesService.getById(id));
    }

    /**
     * 新增sku图片
     */
    @ApiOperation("新增sku图片")
    @PreAuthorize("@ss.hasPermi('sc:images:add')")
    @Log(title = "sku图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SkuImages skuImages) {
        return toAjax(skuImagesService.save(skuImages));
    }

    /**
     * 修改sku图片
     */
    @ApiOperation("修改sku图片")
    @PreAuthorize("@ss.hasPermi('sc:images:edit')")
    @Log(title = "sku图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SkuImages skuImages) {
        return toAjax(skuImagesService.updateById(skuImages));
    }

    /**
     * 删除sku图片
     */
    @ApiOperation("删除sku图片")
    @PreAuthorize("@ss.hasPermi('sc:images:remove')")
    @Log(title = "sku图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(skuImagesService.removeByIds(Arrays.asList(ids)));
    }
}

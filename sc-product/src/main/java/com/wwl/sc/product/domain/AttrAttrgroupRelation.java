package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 属性&属性分组关联对象 pms_attr_attrgroup_relation
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_attr_attrgroup_relation")
public class AttrAttrgroupRelation implements Serializable {
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId
    private Long id;

    /** 属性id */
    @Excel(name = "属性id")
    private Long attrId;

    /** 属性分组id */
    @Excel(name = "属性分组id")
    private Long attrGroupId;

    /** 属性组内排序 */
    @Excel(name = "属性组内排序")
    private Long attrSort;

}

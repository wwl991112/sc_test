package com.wwl.sc.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class ScProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScProductApplication.class, args);
    }

}

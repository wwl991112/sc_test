package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.CommentReplay;
import com.wwl.sc.product.service.ICommentReplayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 商品评价回复关系Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/replay")
@Api(value = "商品评价回复关系控制器", tags = {"商品评价回复关系管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CommentReplayController extends BaseController
{
    private final ICommentReplayService commentReplayService;

    /**
     * 查询商品评价回复关系列表
     */
    @ApiOperation("查询商品评价回复关系列表")
    @PreAuthorize("@ss.hasPermi('sc:replay:list')")
    @GetMapping("/list")
    public TableDataInfo list(CommentReplay commentReplay) {
        startPage();
        List<CommentReplay> list = commentReplayService.list(new QueryWrapper<CommentReplay>(commentReplay));
        return getDataTable(list);
    }

    /**
     * 导出商品评价回复关系列表
     */
    @ApiOperation("导出商品评价回复关系列表")
    @PreAuthorize("@ss.hasPermi('sc:replay:export')")
    @Log(title = "商品评价回复关系", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CommentReplay commentReplay) {
        List<CommentReplay> list = commentReplayService.list(new QueryWrapper<CommentReplay>(commentReplay));
        ExcelUtil<CommentReplay> util = new ExcelUtil<CommentReplay>(CommentReplay.class);
        return util.exportExcel(list, "商品评价回复关系数据");
    }

    /**
     * 获取商品评价回复关系详细信息
     */
    @ApiOperation("获取商品评价回复关系详细信息")
    @PreAuthorize("@ss.hasPermi('sc:replay:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(commentReplayService.getById(id));
    }

    /**
     * 新增商品评价回复关系
     */
    @ApiOperation("新增商品评价回复关系")
    @PreAuthorize("@ss.hasPermi('sc:replay:add')")
    @Log(title = "商品评价回复关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CommentReplay commentReplay) {
        return toAjax(commentReplayService.save(commentReplay));
    }

    /**
     * 修改商品评价回复关系
     */
    @ApiOperation("修改商品评价回复关系")
    @PreAuthorize("@ss.hasPermi('sc:replay:edit')")
    @Log(title = "商品评价回复关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CommentReplay commentReplay) {
        return toAjax(commentReplayService.updateById(commentReplay));
    }

    /**
     * 删除商品评价回复关系
     */
    @ApiOperation("删除商品评价回复关系")
    @PreAuthorize("@ss.hasPermi('sc:replay:remove')")
    @Log(title = "商品评价回复关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(commentReplayService.removeByIds(Arrays.asList(ids)));
    }
}

package com.wwl.sc.product.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wwl.sc.product.domain.SpuComment;

/**
 * 商品评价Service接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
public interface ISpuCommentService extends IService<SpuComment> {

}

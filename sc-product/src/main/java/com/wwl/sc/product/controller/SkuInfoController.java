package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.SkuInfo;
import com.wwl.sc.product.service.ISkuInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * sku信息Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/info")
@Api(value = "sku信息控制器", tags = {"sku信息管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SkuInfoController extends BaseController
{
    private final ISkuInfoService skuInfoService;

    /**
     * 查询sku信息列表
     */
    @ApiOperation("查询sku信息列表")
    @PreAuthorize("@ss.hasPermi('sc:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(SkuInfo skuInfo) {
        startPage();
        List<SkuInfo> list = skuInfoService.list(new QueryWrapper<SkuInfo>(skuInfo));
        return getDataTable(list);
    }

    /**
     * 导出sku信息列表
     */
    @ApiOperation("导出sku信息列表")
    @PreAuthorize("@ss.hasPermi('sc:info:export')")
    @Log(title = "sku信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SkuInfo skuInfo) {
        List<SkuInfo> list = skuInfoService.list(new QueryWrapper<SkuInfo>(skuInfo));
        ExcelUtil<SkuInfo> util = new ExcelUtil<SkuInfo>(SkuInfo.class);
        return util.exportExcel(list, "sku信息数据");
    }

    /**
     * 获取sku信息详细信息
     */
    @ApiOperation("获取sku信息详细信息")
    @PreAuthorize("@ss.hasPermi('sc:info:query')")
    @GetMapping(value = "/{skuId}")
    public AjaxResult getInfo(@PathVariable("skuId") Long skuId) {
        return AjaxResult.success(skuInfoService.getById(skuId));
    }

    /**
     * 新增sku信息
     */
    @ApiOperation("新增sku信息")
    @PreAuthorize("@ss.hasPermi('sc:info:add')")
    @Log(title = "sku信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SkuInfo skuInfo) {
        return toAjax(skuInfoService.save(skuInfo));
    }

    /**
     * 修改sku信息
     */
    @ApiOperation("修改sku信息")
    @PreAuthorize("@ss.hasPermi('sc:info:edit')")
    @Log(title = "sku信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SkuInfo skuInfo) {
        return toAjax(skuInfoService.updateById(skuInfo));
    }

    /**
     * 删除sku信息
     */
    @ApiOperation("删除sku信息")
    @PreAuthorize("@ss.hasPermi('sc:info:remove')")
    @Log(title = "sku信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{skuIds}")
    public AjaxResult remove(@PathVariable Long[] skuIds) {
        return toAjax(skuInfoService.removeByIds(Arrays.asList(skuIds)));
    }
}

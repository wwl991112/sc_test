package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

}

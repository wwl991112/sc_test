package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * spu属性值对象 pms_product_attr_value
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_product_attr_value")
public class ProductAttrValue implements Serializable {
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId
    private Long id;

    /** 商品id */
    @Excel(name = "商品id")
    private Long spuId;

    /** 属性id */
    @Excel(name = "属性id")
    private Long attrId;

    /** 属性名 */
    @Excel(name = "属性名")
    private String attrName;

    /** 属性值 */
    @Excel(name = "属性值")
    private String attrValue;

    /** 顺序 */
    @Excel(name = "顺序")
    private Long attrSort;

    /** 快速展示【是否展示在介绍上；0-否 1-是】 */
    @Excel(name = "快速展示【是否展示在介绍上；0-否 1-是】")
    private Long quickShow;

}

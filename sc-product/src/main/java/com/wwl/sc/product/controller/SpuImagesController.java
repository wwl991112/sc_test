package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.SpuImages;
import com.wwl.sc.product.service.ISpuImagesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * spu图片Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/images")
@Api(value = "spu图片控制器", tags = {"spu图片管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SpuImagesController extends BaseController
{
    private final ISpuImagesService spuImagesService;

    /**
     * 查询spu图片列表
     */
    @ApiOperation("查询spu图片列表")
    @PreAuthorize("@ss.hasPermi('sc:images:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpuImages spuImages) {
        startPage();
        List<SpuImages> list = spuImagesService.list(new QueryWrapper<SpuImages>(spuImages));
        return getDataTable(list);
    }

    /**
     * 导出spu图片列表
     */
    @ApiOperation("导出spu图片列表")
    @PreAuthorize("@ss.hasPermi('sc:images:export')")
    @Log(title = "spu图片", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpuImages spuImages) {
        List<SpuImages> list = spuImagesService.list(new QueryWrapper<SpuImages>(spuImages));
        ExcelUtil<SpuImages> util = new ExcelUtil<SpuImages>(SpuImages.class);
        return util.exportExcel(list, "spu图片数据");
    }

    /**
     * 获取spu图片详细信息
     */
    @ApiOperation("获取spu图片详细信息")
    @PreAuthorize("@ss.hasPermi('sc:images:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(spuImagesService.getById(id));
    }

    /**
     * 新增spu图片
     */
    @ApiOperation("新增spu图片")
    @PreAuthorize("@ss.hasPermi('sc:images:add')")
    @Log(title = "spu图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpuImages spuImages) {
        return toAjax(spuImagesService.save(spuImages));
    }

    /**
     * 修改spu图片
     */
    @ApiOperation("修改spu图片")
    @PreAuthorize("@ss.hasPermi('sc:images:edit')")
    @Log(title = "spu图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpuImages spuImages) {
        return toAjax(spuImagesService.updateById(spuImages));
    }

    /**
     * 删除spu图片
     */
    @ApiOperation("删除spu图片")
    @PreAuthorize("@ss.hasPermi('sc:images:remove')")
    @Log(title = "spu图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(spuImagesService.removeByIds(Arrays.asList(ids)));
    }
}

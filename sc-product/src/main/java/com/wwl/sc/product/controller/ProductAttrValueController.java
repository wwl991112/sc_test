package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.ProductAttrValue;
import com.wwl.sc.product.service.IProductAttrValueService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * spu属性值Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/value")
@Api(value = "spu属性值控制器", tags = {"spu属性值管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ProductAttrValueController extends BaseController
{
    private final IProductAttrValueService productAttrValueService;

    /**
     * 查询spu属性值列表
     */
    @ApiOperation("查询spu属性值列表")
    @PreAuthorize("@ss.hasPermi('sc:value:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductAttrValue productAttrValue) {
        startPage();
        List<ProductAttrValue> list = productAttrValueService.list(new QueryWrapper<ProductAttrValue>(productAttrValue));
        return getDataTable(list);
    }

    /**
     * 导出spu属性值列表
     */
    @ApiOperation("导出spu属性值列表")
    @PreAuthorize("@ss.hasPermi('sc:value:export')")
    @Log(title = "spu属性值", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProductAttrValue productAttrValue) {
        List<ProductAttrValue> list = productAttrValueService.list(new QueryWrapper<ProductAttrValue>(productAttrValue));
        ExcelUtil<ProductAttrValue> util = new ExcelUtil<ProductAttrValue>(ProductAttrValue.class);
        return util.exportExcel(list, "spu属性值数据");
    }

    /**
     * 获取spu属性值详细信息
     */
    @ApiOperation("获取spu属性值详细信息")
    @PreAuthorize("@ss.hasPermi('sc:value:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(productAttrValueService.getById(id));
    }

    /**
     * 新增spu属性值
     */
    @ApiOperation("新增spu属性值")
    @PreAuthorize("@ss.hasPermi('sc:value:add')")
    @Log(title = "spu属性值", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductAttrValue productAttrValue) {
        return toAjax(productAttrValueService.save(productAttrValue));
    }

    /**
     * 修改spu属性值
     */
    @ApiOperation("修改spu属性值")
    @PreAuthorize("@ss.hasPermi('sc:value:edit')")
    @Log(title = "spu属性值", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductAttrValue productAttrValue) {
        return toAjax(productAttrValueService.updateById(productAttrValue));
    }

    /**
     * 删除spu属性值
     */
    @ApiOperation("删除spu属性值")
    @PreAuthorize("@ss.hasPermi('sc:value:remove')")
    @Log(title = "spu属性值", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productAttrValueService.removeByIds(Arrays.asList(ids)));
    }
}

package com.wwl.sc.product.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wwl.sc.product.domain.AttrGroup;

/**
 * 属性分组Service接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
public interface IAttrGroupService extends IService<AttrGroup> {

}

package com.wwl.sc.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wwl.sc.product.domain.Attr;

/**
 * 商品属性Service接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
public interface IAttrService extends IService<Attr> {

}

package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * sku图片对象 pms_sku_images
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_sku_images")
public class SkuImages implements Serializable {
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId
    private Long id;

    /** sku_id */
    @Excel(name = "sku_id")
    private Long skuId;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String imgUrl;

    /** 排序 */
    @Excel(name = "排序")
    private Long imgSort;

    /** 默认图[0 - 不是默认图，1 - 是默认图] */
    @Excel(name = "默认图[0 - 不是默认图，1 - 是默认图]")
    private Long defaultImg;

}

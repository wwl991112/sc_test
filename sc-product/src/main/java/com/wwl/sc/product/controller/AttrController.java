package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.Attr;
import com.wwl.sc.product.service.IAttrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 商品属性Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/attr")
@Api(value = "商品属性控制器", tags = {"商品属性管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AttrController extends BaseController
{
    private final IAttrService attrService;

    /**
     * 查询商品属性列表
     */
    @ApiOperation("查询商品属性列表")
    @PreAuthorize("@ss.hasPermi('sc:attr:list')")
    @GetMapping("/list")
    public TableDataInfo list(Attr attr) {
        startPage();
        List<Attr> list = attrService.list(new QueryWrapper<Attr>(attr));
        return getDataTable(list);
    }

    /**
     * 导出商品属性列表
     */
    @ApiOperation("导出商品属性列表")
    @PreAuthorize("@ss.hasPermi('sc:attr:export')")
    @Log(title = "商品属性", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Attr attr) {
        List<Attr> list = attrService.list(new QueryWrapper<Attr>(attr));
        ExcelUtil<Attr> util = new ExcelUtil<Attr>(Attr.class);
        return util.exportExcel(list, "商品属性数据");
    }

    /**
     * 获取商品属性详细信息
     */
    @ApiOperation("获取商品属性详细信息")
    @PreAuthorize("@ss.hasPermi('sc:attr:query')")
    @GetMapping(value = "/{attrId}")
    public AjaxResult getInfo(@PathVariable("attrId") Long attrId) {
        return AjaxResult.success(attrService.getById(attrId));
    }

    /**
     * 新增商品属性
     */
    @ApiOperation("新增商品属性")
    @PreAuthorize("@ss.hasPermi('sc:attr:add')")
    @Log(title = "商品属性", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Attr attr) {
        return toAjax(attrService.save(attr));
    }

    /**
     * 修改商品属性
     */
    @ApiOperation("修改商品属性")
    @PreAuthorize("@ss.hasPermi('sc:attr:edit')")
    @Log(title = "商品属性", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Attr attr) {
        return toAjax(attrService.updateById(attr));
    }

    /**
     * 删除商品属性
     */
    @ApiOperation("删除商品属性")
    @PreAuthorize("@ss.hasPermi('sc:attr:remove')")
    @Log(title = "商品属性", businessType = BusinessType.DELETE)
	@DeleteMapping("/{attrIds}")
    public AjaxResult remove(@PathVariable Long[] attrIds) {
        return toAjax(attrService.removeByIds(Arrays.asList(attrIds)));
    }
}

package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.Category;
import com.wwl.sc.product.mapper.CategoryMapper;
import com.wwl.sc.product.service.ICategoryService;
import org.springframework.stereotype.Service;

/**
 * 商品三级分类Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

}

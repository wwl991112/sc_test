package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.Category;
import com.wwl.sc.product.service.ICategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 商品三级分类Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/category")
@Api(value = "商品三级分类控制器", tags = {"商品三级分类管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CategoryController extends BaseController
{
    private final ICategoryService categoryService;

    /**
     * 查询商品三级分类列表
     */
    @ApiOperation("查询商品三级分类列表")
    @PreAuthorize("@ss.hasPermi('sc:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(Category category) {
        startPage();
        List<Category> list = categoryService.list(new QueryWrapper<Category>(category));
        return getDataTable(list);
    }

    /**
     * 导出商品三级分类列表
     */
    @ApiOperation("导出商品三级分类列表")
    @PreAuthorize("@ss.hasPermi('sc:category:export')")
    @Log(title = "商品三级分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Category category) {
        List<Category> list = categoryService.list(new QueryWrapper<Category>(category));
        ExcelUtil<Category> util = new ExcelUtil<Category>(Category.class);
        return util.exportExcel(list, "商品三级分类数据");
    }

    /**
     * 获取商品三级分类详细信息
     */
    @ApiOperation("获取商品三级分类详细信息")
    @PreAuthorize("@ss.hasPermi('sc:category:query')")
    @GetMapping(value = "/{catId}")
    public AjaxResult getInfo(@PathVariable("catId") Long catId) {
        return AjaxResult.success(categoryService.getById(catId));
    }

    /**
     * 新增商品三级分类
     */
    @ApiOperation("新增商品三级分类")
    @PreAuthorize("@ss.hasPermi('sc:category:add')")
    @Log(title = "商品三级分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Category category) {
        return toAjax(categoryService.save(category));
    }

    /**
     * 修改商品三级分类
     */
    @ApiOperation("修改商品三级分类")
    @PreAuthorize("@ss.hasPermi('sc:category:edit')")
    @Log(title = "商品三级分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Category category) {
        return toAjax(categoryService.updateById(category));
    }

    /**
     * 删除商品三级分类
     */
    @ApiOperation("删除商品三级分类")
    @PreAuthorize("@ss.hasPermi('sc:category:remove')")
    @Log(title = "商品三级分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{catIds}")
    public AjaxResult remove(@PathVariable Long[] catIds) {
        return toAjax(categoryService.removeByIds(Arrays.asList(catIds)));
    }
}

package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.SkuInfo;
import com.wwl.sc.product.mapper.SkuInfoMapper;
import com.wwl.sc.product.service.ISkuInfoService;
import org.springframework.stereotype.Service;

/**
 * sku信息Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoMapper, SkuInfo> implements ISkuInfoService {

}

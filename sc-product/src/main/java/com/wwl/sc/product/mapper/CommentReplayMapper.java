package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.CommentReplay;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价回复关系Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface CommentReplayMapper extends BaseMapper<CommentReplay> {

}

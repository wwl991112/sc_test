package com.wwl.sc.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.AttrAttrgroupRelation;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性&属性分组关联Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface AttrAttrgroupRelationMapper extends BaseMapper<AttrAttrgroupRelation> {

}

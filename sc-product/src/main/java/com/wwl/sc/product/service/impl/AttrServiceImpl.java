package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.Attr;
import com.wwl.sc.product.mapper.AttrMapper;
import com.wwl.sc.product.service.IAttrService;
import org.springframework.stereotype.Service;

/**
 * 商品属性Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class AttrServiceImpl extends ServiceImpl<AttrMapper, Attr> implements IAttrService {

}

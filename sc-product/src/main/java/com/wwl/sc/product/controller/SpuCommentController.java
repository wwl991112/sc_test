package com.wwl.sc.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.wwl.sc.product.domain.SpuComment;
import com.wwl.sc.product.service.ISpuCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 商品评价Controller
 * 
 * @author wwl
 * @date 2023-12-17
 */
@RestController
@RequestMapping("/sc/comment")
@Api(value = "商品评价控制器", tags = {"商品评价管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SpuCommentController extends BaseController
{
    private final ISpuCommentService spuCommentService;

    /**
     * 查询商品评价列表
     */
    @ApiOperation("查询商品评价列表")
    @PreAuthorize("@ss.hasPermi('sc:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpuComment spuComment) {
        startPage();
        List<SpuComment> list = spuCommentService.list(new QueryWrapper<SpuComment>(spuComment));
        return getDataTable(list);
    }

    /**
     * 导出商品评价列表
     */
    @ApiOperation("导出商品评价列表")
    @PreAuthorize("@ss.hasPermi('sc:comment:export')")
    @Log(title = "商品评价", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpuComment spuComment) {
        List<SpuComment> list = spuCommentService.list(new QueryWrapper<SpuComment>(spuComment));
        ExcelUtil<SpuComment> util = new ExcelUtil<SpuComment>(SpuComment.class);
        return util.exportExcel(list, "商品评价数据");
    }

    /**
     * 获取商品评价详细信息
     */
    @ApiOperation("获取商品评价详细信息")
    @PreAuthorize("@ss.hasPermi('sc:comment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(spuCommentService.getById(id));
    }

    /**
     * 新增商品评价
     */
    @ApiOperation("新增商品评价")
    @PreAuthorize("@ss.hasPermi('sc:comment:add')")
    @Log(title = "商品评价", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpuComment spuComment) {
        return toAjax(spuCommentService.save(spuComment));
    }

    /**
     * 修改商品评价
     */
    @ApiOperation("修改商品评价")
    @PreAuthorize("@ss.hasPermi('sc:comment:edit')")
    @Log(title = "商品评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpuComment spuComment) {
        return toAjax(spuCommentService.updateById(spuComment));
    }

    /**
     * 删除商品评价
     */
    @ApiOperation("删除商品评价")
    @PreAuthorize("@ss.hasPermi('sc:comment:remove')")
    @Log(title = "商品评价", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(spuCommentService.removeByIds(Arrays.asList(ids)));
    }
}

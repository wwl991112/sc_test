package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 品牌对象 pms_brand
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_brand")
public class Brand implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 品牌id */
    @TableId
    private Long brandId;

    /** 品牌名 */
    @Excel(name = "品牌名")
    private String name;

    /** 品牌logo地址 */
    @Excel(name = "品牌logo地址")
    private String logo;

    /** 介绍 */
    @Excel(name = "介绍")
    private String descript;

    /** 显示状态[0-不显示；1-显示] */
    @Excel(name = "显示状态[0-不显示；1-显示]")
    private Long showStatus;

    /** 检索首字母 */
    @Excel(name = "检索首字母")
    private String firstLetter;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

}

package com.wwl.sc.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wwl.sc.product.domain.SpuImages;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片Mapper接口
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Mapper
public interface SpuImagesMapper extends BaseMapper<SpuImages> {

}

package com.wwl.sc.product.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.ProductAttrValue;
import com.wwl.sc.product.mapper.ProductAttrValueMapper;
import com.wwl.sc.product.service.IProductAttrValueService;
import org.springframework.stereotype.Service;

/**
 * spu属性值Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class ProductAttrValueServiceImpl extends ServiceImpl<ProductAttrValueMapper, ProductAttrValue> implements IProductAttrValueService {

}

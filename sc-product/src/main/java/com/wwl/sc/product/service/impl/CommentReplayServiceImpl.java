package com.wwl.sc.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wwl.sc.product.domain.CommentReplay;
import com.wwl.sc.product.mapper.CommentReplayMapper;
import com.wwl.sc.product.service.ICommentReplayService;
import org.springframework.stereotype.Service;



/**
 * 商品评价回复关系Service业务层处理
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Service
public class CommentReplayServiceImpl extends ServiceImpl<CommentReplayMapper, CommentReplay> implements ICommentReplayService {

}

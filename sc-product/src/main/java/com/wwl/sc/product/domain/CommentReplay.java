package com.wwl.sc.product.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 商品评价回复关系对象 pms_comment_replay
 * 
 * @author wwl
 * @date 2023-12-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pms_comment_replay")
public class CommentReplay implements Serializable {
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId
    private Long id;

    /** 评论id */
    @Excel(name = "评论id")
    private Long commentId;

    /** 回复id */
    @Excel(name = "回复id")
    private Long replyId;

}

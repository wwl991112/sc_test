package com.wwl.sc.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScCouponApplication.class, args);
    }

}

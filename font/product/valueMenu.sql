-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu属性值', '3', '1', 'value', 'sc/value/index', 1, 0, 'C', '0', '0', 'sc:value:list', '#', 'admin', sysdate(), '', null, 'spu属性值菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu属性值查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'sc:value:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu属性值新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'sc:value:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu属性值修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'sc:value:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu属性值删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'sc:value:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu属性值导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'sc:value:export',       '#', 'admin', sysdate(), '', null, '');
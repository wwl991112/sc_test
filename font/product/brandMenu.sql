-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('品牌', '3', '1', 'brand', 'sc/brand/index', 1, 0, 'C', '0', '0', 'sc:brand:list', '#', 'admin', sysdate(), '', null, '品牌菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('品牌查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'sc:brand:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('品牌新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'sc:brand:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('品牌修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'sc:brand:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('品牌删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'sc:brand:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('品牌导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'sc:brand:export',       '#', 'admin', sysdate(), '', null, '');
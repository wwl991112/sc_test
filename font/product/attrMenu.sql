-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品属性', '3', '1', 'attr', 'sc/attr/index', 1, 0, 'C', '0', '0', 'sc:attr:list', '#', 'admin', sysdate(), '', null, '商品属性菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品属性查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'sc:attr:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品属性新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'sc:attr:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品属性修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'sc:attr:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品属性删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'sc:attr:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品属性导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'sc:attr:export',       '#', 'admin', sysdate(), '', null, '');
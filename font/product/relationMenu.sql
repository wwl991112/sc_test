-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性&属性分组关联', '3', '1', 'relation', 'sc/relation/index', 1, 0, 'C', '0', '0', 'sc:relation:list', '#', 'admin', sysdate(), '', null, '属性&属性分组关联菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性&属性分组关联查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'sc:relation:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性&属性分组关联新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'sc:relation:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性&属性分组关联修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'sc:relation:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性&属性分组关联删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'sc:relation:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性&属性分组关联导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'sc:relation:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu信息介绍', '3', '1', 'desc', 'sc/desc/index', 1, 0, 'C', '0', '0', 'sc:desc:list', '#', 'admin', sysdate(), '', null, 'spu信息介绍菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu信息介绍查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'sc:desc:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu信息介绍新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'sc:desc:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu信息介绍修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'sc:desc:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu信息介绍删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'sc:desc:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('spu信息介绍导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'sc:desc:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性分组', '3', '1', 'group', 'sc/group/index', 1, 0, 'C', '0', '0', 'sc:group:list', '#', 'admin', sysdate(), '', null, '属性分组菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性分组查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'sc:group:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性分组新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'sc:group:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性分组修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'sc:group:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性分组删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'sc:group:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('属性分组导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'sc:group:export',       '#', 'admin', sysdate(), '', null, '');
package com.wwl.sc.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScWareApplication.class, args);
    }

}

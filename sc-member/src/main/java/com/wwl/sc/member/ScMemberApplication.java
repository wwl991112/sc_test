package com.wwl.sc.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScMemberApplication.class, args);
    }

}
